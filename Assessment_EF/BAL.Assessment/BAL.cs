﻿using System.Reflection.Metadata;
using BusinessObject.Assessment.Model;
using DAL.Assessment;

namespace BAL.Assessment
{
    public class ClassBAL
    {
        DAl dal = new DAl();
        
        public int AddUser(User user)
        {
            dal.AddUser(user);
            return 0;
        }

        public int DeleteUser(int id)
        {
            dal.DeleteUser(id);
            return 1;
        }

        
        public int AddProduct(Product product)
        {
            dal.AddProduct(product);
            return 0;
        }
        public int UpdateProduct(string name, Product product)
        {
            dal.UpdateProduct(name, product);
            return 1;
        }
        public int DeleteProduct(int id)
        {
            dal.DeleteProduct(id);
            return 0;
        }

        
        public int DisplayProduct(Product product)
        {
            dal.DisplayProduct();
            return 0;
        }
       
        public int AddOrder(Order order)
        {
            dal.AddOrder(order);
            return 0;
        }


    }
}
﻿using BusinessObject.Assessment.Model;
using DAL.Assessment.Context;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Assessment
{
    public class DAl
    {
        ShoppingDbContext db = new ShoppingDbContext();




        public int AddUser(User user)
        {
            List<Role> Roles = db.Roles.ToList();
            Console.WriteLine(Roles);
            Console.WriteLine("Choose role id:\n 1 for Supplier\n 2 for Customer");
            int RoleId = Convert.ToInt32(Console.ReadLine());



           Role obj = db.Roles.Find(RoleId);
            user.Role = obj;
            user.Role.roleId = RoleId;
            db.Users.Add(user);
            db.SaveChanges();
            Console.WriteLine("User added!");
            return 0;
        }

        public int DeleteUser(int id)
        {
            User obj = db.Users.Where(x => x.userId == id).FirstOrDefault();
            if (obj != null)
            {
                db.Users.Remove(obj);
                Console.WriteLine("User deleted!");
                db.SaveChanges();
                return 0;
            }
            else
            {
                Console.WriteLine("No such user exists with this id");
                return 1;
            }
        }



            public int AddProduct(Product product)
        {
            db.Products.Add(product);
            db.SaveChanges();
            return 0;

        }
        public int UpdateProduct(string name, Product product)
        {
            Product obj = db.Products.Where(x => x.productName == name).FirstOrDefault();
            if (obj != null)
            {
                if (product.productName.Length > 0)
                    obj.productName = product.productName;
                //if (product.Price > 0)
                //    obj.Price = product.Price;

                if (product.qtyInStock > 0)
                    obj.qtyInStock = product.qtyInStock;

                db.Products.Update(obj);
                db.SaveChanges();
            }
            else
            {
                return 1;
            }

            return 0;

        }


        public int DeleteProduct(int id)
        {
            Product del = db.Products.Where(x => x.productId == id).FirstOrDefault();
            if (del != null)
            {
                db.Products.Remove(del);
                db.SaveChanges();
            }
            return 0;



        }

       
        
        public List<Product> DisplayProduct()
        {
           
            List<Product> list = new List<Product>();
            list = db.Products.ToList();
            foreach (Product temp in list)
            {
                Console.WriteLine($"{temp.productName}--{temp.qtyInStock}");
            }

            return list;
        }




        public int AddOrder(Order order)
        {
            db.Orders.Add(order);
            db.SaveChanges();
            return 0;
        }


    }
}



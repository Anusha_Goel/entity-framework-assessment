﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DAL.Assessment.Migrations
{
    /// <inheritdoc />
    public partial class changeInOrder : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ProductName",
                table: "Orders",
                newName: "OrderName");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "OrderName",
                table: "Orders",
                newName: "ProductName");
        }
    }
}

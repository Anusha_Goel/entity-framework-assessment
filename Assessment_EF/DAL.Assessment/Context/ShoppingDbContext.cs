﻿
using BusinessObject.Assessment.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Assessment.Context
{
    internal class ShoppingDbContext:DbContext
    {
        public ShoppingDbContext()
        {

        }
        public ShoppingDbContext(DbContextOptions<ShoppingDbContext> options) : base(options) { }

        public DbSet<Role> Roles { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Product> Products { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"data source=INW-915;initial catalog=DbShoppingSite;integrated security=true;TrustServerCertificate=True");
        }

    }
}

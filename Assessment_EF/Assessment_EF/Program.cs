﻿using BAL.Assessment;
using BusinessObject.Assessment.Model;
using DAL.Assessment;

using System.Reflection.Metadata;


class Program
{
    public static void Main(string[] args)
    {
        ClassBAL bal = new ClassBAL();
        Console.WriteLine("What role do you want to choose. \n 1.Admin\n 2.Supplier\n 3.Customer\n");
        int choice = Convert.ToInt32(Console.ReadLine());

        switch (choice)
        {

            case 1:
                Console.WriteLine("You have selected Admin.\n");
                Console.WriteLine("  1.Add user");
                Console.WriteLine("  2.Delete user");
                int type = Convert.ToInt32(Console.ReadLine());
                if (type == 1)
                {
                    Console.WriteLine("Enter first name:");
                    string first_Name = Console.ReadLine();
                    Console.WriteLine("Enter last name:");
                    string last_Name = Console.ReadLine();

                    User user = new User()
                    {
                        firstName = first_Name,
                        lastName=last_Name,
                        isActive = true
                    };
                    bal.AddUser(user);
                }
                else if (type == 2)
                {
                    Console.WriteLine("enter user id");
                    int id = Convert.ToInt32(Console.ReadLine());
                    bal.DeleteUser(id);
                }
                break;
            case 2: Console.WriteLine("You have selected Supplier.\n");
                Console.WriteLine("What do you want to do? \n1.Add Product.\n 2.Edit Product. \n 3.Delete Product\n");
                int option= Convert.ToInt32(Console.ReadLine());
                if(option==1)
                {
                    AddProduct();
                }
                else if(option==2)
                {
                    Console.WriteLine("Enter the name of the product to edit");
                    string name = Console.ReadLine();
                    ////Product product = bal.GetProduct(name);
                    //if (product != null)
                    //Console.WriteLine("Enter the new price");
                    //int Price = Convert.ToInt32(Console.ReadLine());
                    //int Price;
                    //Int32.TryParse(Console.ReadLine(), out Price);
                    //Console.WriteLine("Enter Product Description");
                    //string pDes = Console.ReadLine();
                    Console.WriteLine("Enter the new name");
                    string? productName = Console.ReadLine();

                    Console.WriteLine("Enter the new Quantity");
                    int quantity;
                    Int32.TryParse(Console.ReadLine(), out quantity);
                  
                    Product upDatedProduct = new Product()
                    {
                        productName = productName,
                        qtyInStock = quantity
                    };

                    bal.UpdateProduct(name, upDatedProduct);
                }
                else if(option==3)
                {
                    DeleteProduct();
                }
                break;
                case 3: Console.WriteLine("You have selected Customer");
                Console.WriteLine("What do you want? \n 1. See all product \n 2.Place order");
                int opt=Convert.ToInt32(Console.ReadLine());
                if (opt == 1)
                {
                    Console.WriteLine("All Products::::");
                    DisplayProduct();

                }
                else if (opt == 2)
                {
                    Console.WriteLine("Invalid");
                }
                else
                {
                    Console.WriteLine("Invalid");
                }
                break;
            default:
                Console.WriteLine("Invalid choice");
                break;
        }

    }
    public static void AddProduct()
    {
        ClassBAL bal = new ClassBAL();
        Console.WriteLine("Enter Product Name");
        string productName = Console.ReadLine();
        Console.WriteLine("ENter Quantity in stock");
        int qtyInStock = Convert.ToInt32(Console.ReadLine());


        Product product = new Product()
        {
            productName = productName,
           qtyInStock = qtyInStock

        };

        int response = bal.AddProduct(product);
        if (response == 0)
        {
            Console.WriteLine("Product has been added");
            
        }
    }

    
    public static void DeleteProduct()
    {

        ClassBAL bal = new ClassBAL();
        Console.WriteLine("eNTER Product id TO DELETE");
        int ID = byte.Parse(Console.ReadLine());
        int result = bal.DeleteProduct(ID);
        Console.WriteLine("Rceord Deleted");
    }
    public static void DisplayProduct()
    {
        ClassBAL bal = new ClassBAL();
        Product product = new Product();
       bal.DisplayProduct(product);
        
    }
}

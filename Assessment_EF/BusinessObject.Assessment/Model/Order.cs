﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObject.Assessment.Model
{
    public class Order
    {
        public int orderId { get; set; }
        public DateTime orderPlaced { get; set; }

        public string orderName { get; set; }

        
        //public int ProductId { get; set; }
        //UserId
    
        
        //ProductId, User
        public User User { get; set; }
        public Product Product { get; set; }


    }
}

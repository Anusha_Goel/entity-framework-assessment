﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObject.Assessment.Model
{
    public class User
    {
        public int userId { get; set; }
        public string firstName { get; set; }

        public string lastName { get; set; }

        public bool isActive { get; set; }
        
        
        //foreign key
        public Role Role { get; set; }

    }
}
